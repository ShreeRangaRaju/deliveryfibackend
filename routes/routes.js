var express = require('express');
var router = express.Router();
//var Store = require('../models/stores');
//var Product = require('../models/products');

// Add controllers
var productController = require('../controller/productController');
var storeController = require('../controller/storeController');



//GET Requests

//GET All Stores
router.get('/v1/stores/', function(req, res, next){
	storeController.getAllStores(req,res, next);
});

//GET store with a specified Id
router.get('/v1/stores/:id', function(req, res, next){
	storeController.getStoreById(req, res, next);
});

//GET all products
router.get('/v1/products/', function(req, res, next){
	productController.getAllProducts(req, res, next);
});





//POST Requests

//POST Stores
router.post('/v1/stores/', function(req, res){
	storeController.postStores(req, res);
});

//POST or Create Products for a particular store 
router.post('/v1/stores/:id/products/', function(req, res){
	productController.postStoreProducts(req, res);
});


//Return the Router
module.exports = router;