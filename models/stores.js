var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Stores Schema
var storesSchema = new Schema({
	storeName: String,
	address:{ 
		building: String,
		street: String,
		city: String,
		province: String,
		zipcode: String,
		coord: []
	}
});

//Return the model
module.exports = mongoose.model('Store', storesSchema);