var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// Products Schema
var productsSchema = new Schema({
	productName: String,
	price: Number,
	createdBy:{type: Schema.ObjectId, ref: 'Store'}
});

//Return the model
module.exports = mongoose.model('Product', productsSchema);