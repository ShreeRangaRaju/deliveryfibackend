//Dependencies
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var port = process.env.PORT || 8080;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var app = express();

//Datamodels
require('./models/products');
require('./models/stores');

//Set the connection to mongodb
mongoose.connect('mongodb://localhost/test');
mongoose.connection.on('connected', function () {  
	console.log('mongodb connected');
}); 

//Logging and Parsing
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(methodOverride());

//Routes
app.use('/', require('./routes/routes'));	




//Listen
app.listen(port);
console.log('Server is now running on port ' + port + '...');