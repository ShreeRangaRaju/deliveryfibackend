//Handlers for the data model
var mongoose = require('mongoose');
var Store = require('../models/stores');
var Product = require('../models/products');

//GET REQUESTS
exports.getAllProducts = function(req, res, next){
	var query = Product.find({});
	query.exec(function(err, products){
		if(err){res.send(err);}
		res.json(products);
		return next();
	});
}


//POST REQUESTS
exports.postStoreProducts = function(req, res, next){
	var newProduct = new Product(req.body);
	newProduct.createdBy = req.params.id;
	newProduct.save(function(err, newProduct){
		if(err){res.send(err);}
			res.json(newProduct);
			return next();
		});
} 

