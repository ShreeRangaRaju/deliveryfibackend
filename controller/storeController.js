//Handlers for the data model
var mongoose = require('mongoose');
var Store = require('../models/stores');
var Product = require('../models/products');


//GET REQUESTS
exports.getAllStores = function(req, res, next){
	var query = Store.find({});
	query.populate('createdBy').exec(function(err, stores){
		if(err){
			res.send(err);
		}
		res.json(stores);
		return next();
	});
}

exports.getStoreById = function(req, res, next){
	var id = req.params.id;
	var query= Store.findById(id);
	query.populate('createdBy').exec(function(err, stores){
		if (err){res.send(err);}
			res.json(stores);
			return next();
		});
		
}

// POST REQUESTS
exports.postStores = function(req, res, next){
	var newStore = new Store(req.body);
	newStore.save(function(err, newStore){
		if (err){
			res.send(err);
		}
		res.json(newStore);
		return next();
	});
}